# CHANGELOG

| Type    | MR                                                                                  | Description                                        |
|---------|-------------------------------------------------------------------------------------|----------------------------------------------------|
| Feature | https://gitlab.com/gitlab-org/secure/pocs/discussion-automation/-/merge_requests/15 | Add Discoto configuration in YAML code block       |
| Bug     | https://gitlab.com/gitlab-org/secure/pocs/discussion-automation/-/merge_requests/14 | Quick actions weren't stripped from copied threads |
