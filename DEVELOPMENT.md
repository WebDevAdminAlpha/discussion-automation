# Discoto Development Documentation

[[_TOC_]]

## Jump Starts

<details>
<summary><b>I need to add a new quick action</b></summary>
<br/>

Look in `lib/discoto/quick_actions.rb` in the `process()` function for where
the current quick actions are defined.

</details>

<details>
<summary><b>I need to add a new YAML setting</b></summary>
<br/>

Look in `lib/discoto/topic_settings.rb` to see where the YAML schemas and parsing
is defined.

The `SCHEMA` attributes define the schema and property defaults. Helper functions
construct a full default value for the schema - this is stored in `DEFAULT_OPTIONS`.

</details>

<details>
<summary><b>I need to change the ~"discoto-beta" behavior</b></summary>
<br/>

The version of discoto that runs on issuables that have the ~"discoto-beta" tag
is the [`beta` branch of discoto](https://gitlab.com/gitlab-org/secure/pocs/discussion-automation/-/tree/beta).

Previously, I have had a branch with a specific feature (`feature_branch_1`),
that I am working on. I then set the `beta` ref to point to `feature_branch_1`
and force push `beta` up to GitLab:

```bash
git checkout feature_branch_1
# do development, commit changes, etc.
git push origin feature_branch_1
# forcefully change the commit that beta points to
git branch -f beta feature_branch_1
# forcefully push the new beta branch up to GitLab
git push -f origin beta
```

</details>

<details>
<summary><b>I need local tests to run faster</b></summary>
<br/>

The local test script in `bin/run_docker_tests.sh` has a `--no-kill` option
that will not kill your local, GitLab docker container once it has been
initialized.

Be sure to include `--no-kill` each time `bin/run_docker_tests.sh` is run if you
desire this behavior!

</details>

<details>
<summary><b>Why don't we run tests in CI?</b></summary>
<br/>

I was unable to run tests in CI successfully. It shouldn't be too hard, especially
since we are already testing a local GitLab instance via docker.

If you want to try this at some point in the future, it may work to make
a GitLab container run as a service that the test code then interacts with. This
should roughly mirror the local testing process with `bin/run_docker_tests.sh`.
</details>

<details>
<summary><b>An issuable in a project/group isn't seen by Discoto</b></summary>
<br/>

Check the [discoto-runner](https://gitlab.com/gitlab-org/secure/pocs/discoto-runner) project's [scheduled pipelines](https://gitlab.com/gitlab-org/secure/pocs/discoto-runner/-/pipeline_schedules).

Make sure the relevant project or group is added to the comma-separated list in
either `DISCOTO_PROJECTS` or `DISCOTO_GROUPS`, respectively.

Remember:

* Epics are loaded from `DISCOTO_GROUPS`
* Issues and merge requests are loaded from `DISCOTO_PROJECTS`
</details>

## Running Locally

Discoto can be run locally by:

* Copying the `bin/.env.example` file to `bin/.env`
* Filling in the desired variables
* Running `bin/run_local.sh`

`bin/run_local.sh` loads the defined environment variables in `bin/.env`
and performs one iteration of discoto.

A few notes about this:

* You probably want to only run this against:
    * Your local GitLab instance (run with GDK/etc)
    * A narrow list of `DISCOTO_PROJECTS` and `DISCOTO_GROUPS`
* This is very useful when interactively debugging an issue with pry

## Testing

Discoto is tested locally via the `bin/run_docker_tests.sh` script. See its
`--help` option:

```bash
$> bin/run_docker_tests.sh --help
bin/run_docker_tests.sh [--no-kill]

  Run the discoto tests against a local gitlab/gitlab-ee:latest docker
  container

[>>>] Cleaning up
```

Example output of `bin/run_docker_tests.sh`:

```
bin/run_docker_tests.sh --no-kill
[>>>] Will not kill docker container (can be re-used)
[>>>] Retrieving token
[>>>] GitLab should be ready
[>>>] Pasteable config into bin/.env:
[>>>]
export DISCOTO_GITLAB_ENDPOINT=http://172.17.0.2
export DISCOTO_GITLAB_TOKEN=XyrCRvtXTJSbriy_FYam
export DISCOTO_PROJECTS=test-group/test-project
export DISCOTO_GROUPS=test-group
export DISCOTO_ISSUABLE_ASSIGNEE=
export DISCOTO_ISSUABLE_AUTHOR=
export DISCOTO_ISSUABLE_LABELS=discoto
[>>>]
[>>>] Running tests on add_documentation @ 90e7e4dc8a3cfafd03e9524e4335c418e0324a2e
[>>>] WARNING:
[>>>] WARNING: You have uncommitted changes
[>>>]   ╭──────
[>>>]   │  M DEVELOPMENT.md
[>>>]   ╰──────
[>>>] WARNING:
[>>>] WARNING:
..........................................................

Finished in 2 minutes 12.8 seconds (files took 0.40891 seconds to load)
59 examples, 0 failures

Coverage report generated for RSpec to /home/james/ws/dev/gitlab/discussion-automation/coverage. 720 / 823 LOC (87.48%) covered.
[>>>] Cleaning up
[>>>] Cleaning up
```

Notice that the test output includes:

* The local URL of the docker container (useful for manual debugging)
* The API token of the local testing GitLab instance
* The current SHA the test ran on
* A list of uncomitted changes

The SHA and uncommitted changes are included in the test script output to help
validate tests in the merge request. The default description for merge requests
in this project requires that the full test output be pasted into the MR
description.

### Test Setup Details

This will start a new GitLab container and initialize it with a
root password and API token that is then used during tests.

As part of `bin/run_docker_tests.sh`, the API token generated during
initialization is saved to `/tmp/root_token.txt` inside the container:

```bash
DOCKER_ROOT_TOKEN_PATH="/tmp/root_token.txt"
# ...
function init_gitlab {
    log "Initializing GitLab"

    log "Initializing GitLab api token, group, and project"
    log docker exec -i "${CONTAINER_ID}" gitlab-rails console
    (
        cat <<EOF | docker exec -i "${CONTAINER_ID}" gitlab-rails console
        root = User.find(1)
        token = PersonalAccessToken.new(user: root, ...)
        token.save!
        File.write('${DOCKER_ROOT_TOKEN_PATH}', token.token)
        # ...
EOF
    ) 2>&1 | indent

    log "Sleeping for a minute before continuing"
    sleep 60
}
```

The saved API token value is then read and used to configure discoto
when it runs its tests:

```bash
function retrieve_token {
    docker exec "$CONTAINER_ID" cat "$DOCKER_ROOT_TOKEN_PATH"
}
# ...
ROOT_TOKEN=$(retrieve_token)
# ...
export DISCOTO_GITLAB_TOKEN="${ROOT_TOKEN}"
# ...
bundler exec rspec "$@"
```

### `--no-kill` Option

During local testing it does not make sense to create and initialize a fresh
instance of GitLab each time the tests run.

The `--no-kill` option prevents the test script from killing the initialized
docker container upon exit, greatly speeding up the execution time of the tests.

## Architecture

```bash
lib
├── discoto
│   ├── items
│   │   ├── inline_topic.rb
│   │   ├── main_point.rb
│   │   ├── thread.rb
│   │   └── topic.rb
│   ├── cacher.rb
│   ├── cli.rb
│   ├── config.rb
│   ├── loader.rb
│   ├── log.rb
│   ├── meta.rb
│   ├── mixins
│   │   ├── json_schema_helper.rb
│   │   ├── summary.rb
│   │   └── topic_tree.rb
│   ├── quick_actions.rb
│   ├── topic_settings.rb
│   ├── utils.rb
│   └── version.rb
└── discoto.rb
```

### `lib/discoto/items`

```bash
lib
└── discoto
    └── items
        ├── inline_topic.rb
        ├── main_point.rb
        ├── thread.rb
        └── topic.rb
```

`items` contains the main objects used by Discoto:

* `Topic` - An issuable-based topic (a stand-alone epic, issue, or merge request)
* `InlineTopic` - A discussion-based sub-topic of an issuable-based topic
* `MainPoint` - Each declared main point of a topic
* `Thread` - One discussion within an issuable. A thread may become an `InlineTopic` if a topic is declared.

**Note**: `lib/discoto/topic.rb` also contains the logic to break cyclic discussions.
See the `break_cycles!` function.

### `lib/discoto/cacher.rb`

Caches loaded objects so that they are only loaded once. This is critical
when doing the initial loading of topics and recursing into linked sub topics.

Example usage of this can be found frequently in the `loader.rb` file:

```ruby
class Loader
  include Cacher
  
  def initialize(client, config)
    cache_init
  end
  
  # ...
  
  def load_epic(epic_iid, group_id)
    group_id = ensure_group_id(group_id)
    item_key = cache_issuable_key(epic_iid, group_id)
    cache(:epics, item_key) do
      IssuableWrapper.new('epic', @client.epic(group_id, epic_iid))
    end
  end

  # ...

end
```

### `lib/discoto/loader.rb`

This defines a simple wrapper around the GitLab API to load items.

Effort is made to make loading and editing topic-related items easier in a way
that doesn't require directly knowing if the topic is an inline topic, an issue,
merge request, or epic.

### `lib/discoto/cli.rb`

This is the main entrypoint of discoto and is what runs when `discoto --help`
is run on the command line.

This would be a good place to hook in extra analytics.

### `lib/discoto/meta.rb`

This module takes care of inserting and updating metadata into text.

Metadata (as used by discoto) is arbitrary data embedded within OR marked by HTML
comments. Each metadata type has its own tag.

#### Embedded Within HTML Comments (Invisible)

For example, the metadata below is embedded **within** HTML comments. It is
inserted in the `Discussion Continues` response when discoto responds to a
`/discuss sub-topic TITLE` quick action. Whitespace has been added for
readability:

```
<!-- discoto:
    {
       "sub_topic" : {
          "container_id" : 26576783,
          "container_type" : "project",
          "iid" : 2,
          "issuable_type" : "issue",
          "type" : "topic",
          "url" : "https://gitlab.com/d0c-s4vage-tests/test_comments/-/issues/2"
       }
    }
-->
```

The metadata injected into the response Note text is what creates the link
between the current (parent) topic and the new (child) topic. Discoto looks for
these links when recursively loading topics and uses them to build a graph
of topics.

You may have the thought "what happens if a cyclic graph is created?" Discoto
detects cyclic graphs and breaks them by severing a link in the cycle. See the
`break_cycles!` function in `lib/discoto/topic.rb`.

#### Marked by HTML Comments (Visible)

The example below uses HTML comments to mark **visible** data within text.
This is the discoto topic settings YAML that is added beneath the
auto summary section:

~~~
<!-- start-discoto-topic-settings --><details>
<summary>Discoto Settings</summary>

<br/>

```yaml
---
summary:
  max_items: -1
  sort_by: created
  sort_direction: ascending

```

See the [settings schema](https://gitlab.com/gitlab-org/secure/pocs/discussion-automation#settings-schema) for details.

</details>
<!-- end-discoto-topic-settings -->
~~~

### `lib/discoto/quick_actions.rb`

Loaded topics use this module to process any embedded quick actions they may
contain.

**Note**: There is potential for security issues to arise with quick actions. Be
careful!

### `lib/discoto/config.rb`

This module takes care of configuring discoto. Options are either specified
on the command line or via environment variables.

## Future Plans

* Refactor the auto-summary presentation from the logic - #24
* Expand on the topic settings YAML - #25
* New sort option to sort summary - #26
* Retroactively declare a discussion to be a topic with a title - #22
