#!/usr/bin/env bash

set -e

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
CONTAINER_ID=""
LOG_PREFIX="[>>>]"
CONTAINER_NAME="discoto-testing"
DOCKER_ROOT_TOKEN_PATH="/tmp/root_token.txt"
GITLAB_CONTAINER_TAG="13.10.0-ee.0"

# OPTIONS
NO_KILL=false
FORCE_CLEANUP=false

function log {
    echo "${LOG_PREFIX} $@"
}

function get_existing_container {
    docker ps | grep "$CONTAINER_NAME" | awk '{print $1}'
}

function indent {
    log '  ╭──────'
    sed "s/^/${LOG_PREFIX}   │ /g"
    log '  ╰──────'
}

function cleanup {
    log "Cleaning up"
    if [ ! -z "$CONTAINER_ID" ] && [ "$NO_KILL" = false ] ; then
        (
            echo docker kill "${CONTAINER_ID}"
            docker kill "${CONTAINER_ID}"
        ) | indent
    fi
}
trap cleanup SIGHUP SIGINT SIGTERM EXIT

function launch_gitlab_container {
    docker run \
      -d \
      --rm \
      $PORTS \
      --name "$CONTAINER_NAME" \
      --env GITLAB_OMNIBUS_CONFIG="require 'socket'; gitlab_rails['initial_root_password'] = 'password'; external_url 'http://' + Socket.ip_address_list.filter{|p| !p.ipv4_loopback?}.first.ip_address" \
      gitlab/gitlab-ee:"$GITLAB_CONTAINER_TAG"
}

function wait_for_gitlab {
    count=0
    max=60
    echo "Checking http://${IP_ADDR}"
    while true ; do
        count=$((count + 1))
        if [ $count -eq $max ] ; then
            exit 1
        fi
        curl -s --fail "http://${IP_ADDR}/users/sign_in" 2>&1 >/dev/null && break || echo "GitLab is not up yet, retrying ($count/$max)"
        sleep 10
    done
}

function init_gitlab {
    log "Initializing GitLab"

    log "Initializing GitLab api token, group, and project"
    log docker exec -i "${CONTAINER_ID}" gitlab-rails console
    (
        cat <<EOF | docker exec -i "${CONTAINER_ID}" gitlab-rails console
        root = User.find(1)
        token = PersonalAccessToken.new(
            user: root,
            name: 'token',
            revoked: false,
            expires_at: 5.days.from_now,
            scopes: ['api']
        )
        token.save!
        File.write('${DOCKER_ROOT_TOKEN_PATH}', token.token)
        test_group = Group.new(owner: root, name: 'test-group', path: 'test-group')
        test_group.save!
        test_group.add_owner(root)
        test_project = Project.new(creator: root, name: 'test-project', group: test_group, path: 'test-project')
        test_project.save!
        test_project.create_repository
        Files::CreateService.new(test_project, root, {
            branch_name: 'master',
            commit_message: 'Initial commit',
            file_path: 'README.md',
            file_content: 'README CONTENTS',
        }).execute
EOF
    ) 2>&1 | indent

    log "Sleeping for a minute before continuing"
    sleep 60
}

function retrieve_token {
    docker exec "$CONTAINER_ID" cat "$DOCKER_ROOT_TOKEN_PATH"
}

CONTAINER_ID=$(get_existing_container)

while [ $# -gt 0 ] ; do
    next_arg="$1"
    shift

    case $next_arg in
        --help)
            cat <<EOF
$0 [--no-kill]

  Run the discoto tests against a local gitlab/gitlab-ee:latest docker
  container

EOF
            NO_KILL=true
            exit 1
            ;;

        --no-kill)
            log "Will not kill docker container (can be re-used)"
            NO_KILL=true
            ;;

        --cleanup|--clean|--rm)
            log "Forcefully cleaning up"
            NO_KILL=false
            exit 0
            ;;

        *)
            break
            ;;
    esac
done

SHOULD_INIT=false
if [ -z "$CONTAINER_ID" ] ; then
    log "No existing GitLab test containers found"
    log "Launching GitLab container in the background"
    CONTAINER_ID=$(launch_gitlab_container)
    SHOULD_INIT=true
fi

IP_ADDR=$(docker inspect -f '{{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}}' "$CONTAINER_ID")

if [ $SHOULD_INIT = true ] ; then
    wait_for_gitlab 2>&1 | indent
    init_gitlab
fi

log "Retrieving token"
ROOT_TOKEN=$(retrieve_token)
if [ -z "${ROOT_TOKEN}" ] ; then
    echo "Could not create root token"
    exit 1
fi

log "GitLab should be ready"
log "Pasteable config into bin/.env: "
log

echo export DISCOTO_GITLAB_ENDPOINT="http://${IP_ADDR}"
export DISCOTO_GITLAB_ENDPOINT="http://${IP_ADDR}"
echo export DISCOTO_GITLAB_TOKEN="${ROOT_TOKEN}"
export DISCOTO_GITLAB_TOKEN="${ROOT_TOKEN}"
echo export DISCOTO_PROJECTS="test-group/test-project"
export DISCOTO_PROJECTS="test-group/test-project"
echo export DISCOTO_GROUPS="test-group"
export DISCOTO_GROUPS="test-group"
echo export DISCOTO_ISSUABLE_ASSIGNEE=""
export DISCOTO_ISSUABLE_ASSIGNEE=""
echo export DISCOTO_ISSUABLE_AUTHOR=""
export DISCOTO_ISSUABLE_AUTHOR=""
echo export DISCOTO_ISSUABLE_LABELS="discoto"
export DISCOTO_ISSUABLE_LABELS="discoto"

log
log "Running tests on $(git rev-parse --abbrev-ref HEAD) @ $(git rev-parse HEAD)"

uncommitted_changes=$(git status -uno --porcelain)
if [ ! -z "$uncommitted_changes" ] ; then
    log "WARNING:"
    log "WARNING: You have uncommitted changes"
    echo "$uncommitted_changes" | indent
    log "WARNING:"
    log "WARNING:"
fi

bundler exec rspec "$@"

cleanup
