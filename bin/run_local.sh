#!/usr/bin/env bash


DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null && pwd )"
THIS_PROG="$0"

if [ ! -f "${DIR}/.env" ] ; then
    echo ".env file not found. Copy ${DIR}/.env.example to ${DIR}/.env and fill in missing values:"
    echo
    echo "    cp ${DIR}/.env.example ${DIR}/.env"
    echo
    exit 1
fi

source "${DIR}/.env"

cd "${DIR}/.."
bundle exec discoto "$@"
