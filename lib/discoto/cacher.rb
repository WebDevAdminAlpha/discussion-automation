# frozen_string_literal: true

module Discoto
  module Cacher
    def cache(name, key, &block)
      @cache[name] ||= {}
      return @cache[name][key] if @cache[name].key?(key)

      res = block.call
      @cache[name][key] = res
      res
    end

    def cached(name, key)
      (@cache[name] || {})[key]
    end

    def cached_or_fail(name, key, msg)
      res = cached(name, key)
      raise msg if res.nil?

      res
    end

    def cache_add(name, key, val)
      @cache[name] ||= {}
      @cache[name][key] = val
      val
    end

    def cache_init
      @cache = {}
    end
    alias cache_clear! cache_init

    def cache_data
      @cache
    end
  end
end
