# frozen_string_literal: true

require 'gitlab'

require 'discoto/config'
require 'discoto/loader'
require 'discoto/items/topic'
require 'discoto/log'

module Discoto
  module CLI
    def self.run
      begin
        Discoto::Config.parse_args
      rescue Discoto::Config::Error => e
        puts e.to_s
        exit 1
      end
      Discoto::Config.show_config_safe

      client = Gitlab.client(
        endpoint: "#{Config::CONFIG[:gitlab_endpoint]}/api/v4",
        private_token: Config::CONFIG[:gitlab_token]
      )

      loader = Loader.new(client, Config::CONFIG)
      topics = loader.load_topics
      puts "Loaded #{topics.count} initial topics"
      topics = topics.map do |topic|
        begin # rubocop:disable Style/RedundantBegin
          topic.process
        rescue StandardError => e
          puts "Could not process topic: #{topic}"
          puts "    #{e}"
          puts e.backtrace.map { |l| "    #{l}" }.join("\n")
          []
        end
      end.flatten

      topics.map(&:break_cycles!)

      puts "Expanded to #{topics.count} total topics"

      topics.each do |topic|
        topic.update_summary
      rescue StandardError => e
        puts "Could not update topic summary: #{topic}"
        puts "    #{e}"
        puts e.backtrace.map { |l| "    #{l}" }.join("\n")
      end
    end
  end
end
