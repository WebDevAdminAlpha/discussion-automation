# frozen_string_literal: true

require 'logger'
require 'optparse'

require_relative 'log'
require_relative 'utils'

module Discoto
  # config module for Discoto
  module Config
    class Error < StandardError; end

    DEFAULT_LABEL = 'discoto'

    CONFIG = { # rubocop:disable Style/MutableConstant
      gitlab_endpoint: Utils.default_env('DISCOTO_GITLAB_ENDPOINT', 'https://gitlab.com'),
      gitlab_token: Utils.default_env('DISCOTO_GITLAB_TOKEN', nil),
      projects: Utils.default_env('DISCOTO_PROJECTS', '').split(',').map(&:strip),
      groups: Utils.default_env('DISCOTO_GROUPS', '').split(',').map(&:strip),
      recurse: Utils.default_env('DISCOTO_RECURSE', 'false').downcase == 'true',
      assignee: Utils.default_env('DISCOTO_ISSUABLE_ASSIGNEE', ''),
      author: Utils.default_env('DISCOTO_ISSUABLE_AUTHOR', ''),
      labels: Utils.default_env('DISCOTO_ISSUABLE_LABELS', '').split(',').map(&:strip)
    }

    def self.comma_sepd(val)
      val.split(',').map(&:strip)
    end

    # print settings that are safe to print
    def self.show_config_safe
      safe_keys = %i[
        gitlab_endpoint
        labels
        projects
        groups
        assignee
        author
      ]
      puts 'Config:'
      safe_keys.each do |key|
        puts format('  %<key>-25s %<value>s', key: key, value: CONFIG[key])
      end
    end

    def self.parse_args
      OptionParser.new do |opts|
        opts.banner = 'Usage: discoto.rb [options]'
        opts.on('--gitlab-endpoint ENDPOINT', 'GitLab endpoint (DISCOTO_GITLAB_ENDPOINT)')
        opts.on('--gitlab-token TOKEN', 'GitLab token (DISCOTO_GITLAB_TOKEN)')
        opts.on('--projects PROJECT', 'Comma-separate project paths for issues and MRs (DISCOTO_PROJECTS)') do |projects|
          CONFIG[:projects] = comma_sepd(projects)
        end
        opts.on('--groups GROUPS', 'Comma-separated group paths for epics (DISCOTO_GROUPS)') do |groups|
          CONFIG[:groups] = comma_sepd(groups)
        end
        opts.on('-r', '--[no-]recurse', 'Recurse into projects from the groups to find topics') do |recurse|
          CONFIG[:recurse] = recurse
        end
        opts.on('--assignee', 'The assignee username to filter on (DISCOTO_ISSUABLE_ASSIGNEE)')
        opts.on('--author', 'The author username to filter on (DISCOTO_ISSUABLE_AUTHOR)')
        opts.on('--labels', 'Comma-separated filter labels (DISCOTO_ISSUABLE_LABELS)') do |labels|
          CONFIG[:labels] = comma_sepd(labels)
        end
        opts.on('-d', '--debug', 'Run in debug mode') do |_d|
          Logging.log_level = Logger::DEBUG
        end
        opts.on('-h', '--help', 'Print help message')
      end.parse!(into: CONFIG)

      # command-line arguments are stored as CONFIG['email-from'.to_sym] instead
      # of CONFIG[:email_from]. This transforms all parsed cmd-line args into the
      # underscore version
      CONFIG.each do |k, v|
        next unless k.to_s =~ /\w*-\w*/

        CONFIG[k.to_s.gsub('-', '_').to_sym] = v
      end

      CONFIG[:labels] = [DEFAULT_LABEL] if CONFIG[:labels].empty?

      errors = []
      errors << 'DISCOTO_GITLAB_ENDPOINT must be set' if CONFIG[:gitlab_endpoint].to_s.empty?
      errors << 'DISCOTO_GITLAB_TOKEN must be set' if CONFIG[:gitlab_token].to_s.empty?

      if (CONFIG[:projects].count + CONFIG[:groups].count).zero?
        errors << 'DISCOTO_PROJECTS and/or DISCOTO_GROUPS must be set'
      end

      unless errors.count.zero?
        raise Error, "Configuration Error:\n\n#{errors.join("\n")}"
      end

      CONFIG[:gitlab_endpoint] = CONFIG[:gitlab_endpoint].delete_suffix('/')

      CONFIG
    end
  end
end
