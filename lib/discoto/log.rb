# frozen_string_literal: true

require 'logger'

# Logging class
module Discoto
  module Logging
    class << self
      attr_writer :log_level
      def log_level
        @log_level || Logger::INFO
      end
    end

    attr_accessor :logger

    def log
      if @logger.nil?
        log_name = @log_name || self.class.name

        @logger = Logger.new(STDOUT)
        @logger.level = Logging.log_level
        orig_formatter = Logger::Formatter.new
        @logger.formatter = proc { |severity, datetime, progname, msg|
          orig_formatter.call(severity, datetime, progname, "#{log_name}: #{msg}")
        }
      end
      @logger
    end
  end
end
