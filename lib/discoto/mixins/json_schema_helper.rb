# frozen_string_literal: true

require 'json-schema'

module Discoto
  module Mixins
    module JsonSchemaHelper
      def validate_and_insert_defaults(schema, data)
        JSON::Validator.fully_validate(schema, data, insert_defaults: true)
      end

      def create_defaults(schema)
        options = {}
        validate_and_insert_defaults(schema, options)
        options
      end

      def object(required: nil, default: nil, **properties)
        result = {
          'type' => 'object',
          # enforce string keys everywhere
          'properties' => properties.transform_keys(&:to_s)
        }

        result['required'] = required unless required.nil?
        result['default'] = default unless default.nil?

        result
      end

      def int(default: nil)
        result = { 'type' => 'integer' }
        result['default'] = default unless default.nil?

        result
      end

      def string(enum: nil, default: nil)
        result = { 'type' => 'string' }
        result['enum'] = enum unless enum.nil?
        result['default'] = default unless default.nil?

        result
      end
    end
  end
end
