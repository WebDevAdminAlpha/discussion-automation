# frozen_string_literal: true

module Discoto
  module Mixins
    module TopicTree
      def root_topic
        return nil if @parent_topic.nil?

        curr = @parent_topic
        curr = curr.parent_topic until curr.nil? || curr.parent_topic.nil?
        curr
      end
    end
  end
end
