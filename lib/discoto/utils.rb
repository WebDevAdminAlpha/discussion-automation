# frozen_string_literal: true

# Utility module
module Discoto
  module Utils
    class << self
      def default_env(key, default)
        val = ENV[key]
        # gitlab-ci quirk about variables - variables must be explicitly copied into
        # the variables for a new job. if the variable is undefined, the value of
        # the copied (empty) variable is '$VARIABLE'
        return default if ["$#{key}", nil, ''].include?(val)

        val
      end
    end
  end
end
