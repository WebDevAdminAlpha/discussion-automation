# frozen_string_literal: true

require 'gitlab'
require 'rspec'
require 'rspec/mocks'

require 'discoto/config'
require 'discoto/loader'

require_relative 'utils'
require_relative 'discoto_setup'

describe 'Loader' do
  include_context 'discoto setup'

  context 'when loading topics' do
    it 'loads existing issues' do
      issues = 5.times.map do |idx|
        TestUtils.new_issue(client, test_project, title: "Issue #{idx}")
      end

      topics = loader.load_topics
      expect(topics.count).to eq(issues.count)

      topic_titles = Set.new(topics.map(&:title))
      issue_titles = Set.new(issues.map(&:title))
      expect(topic_titles).to eq(issue_titles)
    end

    it 'loads existing merge requests' do
      mrs = 5.times.map do |idx|
        TestUtils.new_mr(client, test_project, title: "Mr #{idx}")
      end

      topics = loader.load_topics
      expect(topics.count).to eq(mrs.count)

      topic_titles = Set.new(topics.map(&:title))
      mr_titles = Set.new(mrs.map(&:title))
      expect(topic_titles).to eq(mr_titles)
    end

    it 'loads existing epics' do
    end
  end

  context 'when loading discussion' do
    it 'works with issue discussions' do
      issue = TestUtils.new_issue(client, test_project, threads: [
        [ 'note1', 'note2', 'note3' ],
      ])

      issuable = loader.load_issue(issue.iid, test_project.id)
      discussions = loader.load_discussions(issuable)

      expect(discussions.count).to eq(1)
      expect(discussions[0].notes.count).to eq(3)
      expect(discussions[0].issuable).to eq(issuable)
    end

    it 'works with merge request discussions' do
      mr = TestUtils.new_mr(client, test_project, threads: [
        [ 'note1', 'note2', 'note3' ],
      ])

      issuable = loader.load_merge_request(mr.iid, test_project.id)
      discussions = loader.load_discussions(issuable)

      expect(discussions.count).to eq(1)
      expect(discussions[0].notes.count).to eq(3)
      expect(discussions[0].issuable).to eq(issuable)
    end
  end

  context 'when caching' do
    context 'after loading multiple items' do
      let (:all_topics) do
        issue1 = TestUtils.new_issue(client, test_project, threads: [
          [ '/discuss sub-topic issue2' ]
        ])

        topics = loader.load_topics
        expect(topics.count).to eq(1)
        topic1, topic2 = topics.first.process
        topic1.create_thread("A").add_note('/discuss sub-topic issue3')

        loader.cache_clear!

        topics = loader.load_topics
        expect(topics.count).to eq(1)
        topic1, topic2, topic3 = topics.first.process
        topic3.create_thread("A").add_note("/discuss link #{topic2.url}")

        loader.cache_clear!

        new_mr = TestUtils.new_mr(client, test_project, title: 'MrTopic', threads: [
          [ '/discuss sub-topic issue4' ],
        ])
        new_mr2 = TestUtils.new_mr(client, test_project, title: 'MrTopic2', threads: [
          [ '/discuss sub-topic issue5' ],
        ])
        topics = loader.load_topics
        expect(topics.count).to eq(3)
        topics.map(&:process)

        loader.cache_clear!

        topics = loader.load_topics
        all_topics = topics.map(&:process).flatten
        all_topics.map(&:break_cycles!)
        all_topics.map(&:update_summary)
        all_topics
      end

      describe 'cache keys' do
        it 'uses consistent topic keys' do
          # create everything
          all_topics

          cache = loader.cache_data

          expected_keys = []
          all_topics.map do |topic|
            cinfo = loader.container_info(topic.issuable)
            issuable_type = topic.issuable.type
            expected_keys << "#{cinfo.type}-#{cinfo.id}-#{issuable_type}-#{topic.issuable.iid}"
          end
          expect(expected_keys.count).to eq(7)

          expect(Set.new(cache[:topics].keys)).to eq(Set.new(expected_keys))
        end

        it 'uses consistent issue keys' do
          # create everything
          all_topics

          cache = loader.cache_data

          expected_keys = []
          all_topics.map do |topic|
            cinfo = loader.container_info(topic.issuable)
            next unless topic.issuable.type == "issue"

            expected_keys << "#{cinfo.id}-#{topic.issuable.iid}"
          end

          expect(Set.new(cache[:issues].keys)).to eq(Set.new(expected_keys))
        end

        it 'uses consistent merge request keys' do
          all_topics

          cache = loader.cache_data

          expected_keys = []
          all_topics.map do |topic|
            cinfo = loader.container_info(topic.issuable)
            next unless topic.issuable.type == "merge_request"

            expected_keys << "#{cinfo.id}-#{topic.issuable.iid}"
          end

          expect(Set.new(cache[:merge_requests].keys)).to eq(Set.new(expected_keys))
        end

        it 'uses consistent group keys' do
          all_topics
          loader.load_group(test_group.id)
          loader.load_group(test_group.path)

          expect(loader.cache_data[:groups].keys).to eq([test_group.full_path])
          expect(loader.cache_data[:group_ids].keys).to eq([test_group.id])
        end

        it 'uses consistent project keys' do
          all_topics
          loader.load_project(test_project.id)
          loader.load_project(test_project.path_with_namespace)
          loader.load_project(test_project_private.id)
          loader.load_project(test_project_private.path_with_namespace)

          expect(loader.cache_data[:projects].keys).to eq([
            test_project.path_with_namespace,
            test_project_private.path_with_namespace
          ])
          expect(loader.cache_data[:project_ids].keys).to eq([
            test_project.id,
            test_project_private.id
          ])
        end

        it 'uses consistent key names' do
          all_topics
          loader.load_group(test_group.id)
          loader.load_group(test_group.path)
          loader.load_project(test_project.id)
          loader.load_project(test_project.path_with_namespace)

          expected_keys = Set.new([
            :initial_epics,
            :initial_issues,
            :initial_merge_requests,

            :groups,
            :group_ids,
            :projects,
            :project_ids,

            :topics,

            :issues,
            :merge_requests,

            :discussions,
          ])

          expect(Set.new(loader.cache_data.keys)).to eq(expected_keys)
        end
      end
    end
  end
end
