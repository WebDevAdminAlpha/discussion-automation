# frozen_string_literal: true

require 'rspec'
require 'rspec/mocks'

require 'discoto/quick_actions'

require_relative 'discoto_setup'

describe 'QuickActions' do
  include_context 'discoto setup'

  context 'when processing a thread' do
    it 'correctly splits quick actions into parts' do
      test_url = 'https://some.tld/path/discussions/-/more/discussions/test'
      issue = TestUtils.new_issue(
        client,
        test_project,
        title: 'TestTitle',
        threads: [
          [ "/discuss link #{test_url}" ],
        ],
      )

      topics = loader.load_topics
      expect(topics.count).to eq(1)

      topic = topics.first
      thread = topic.threads.first
      quick_actions = Discoto::QuickActions.new(loader)

      link_called = false
      allow(quick_actions).to receive(:handle_link_issuable) do |_note, thread, args|
        expect(args).to eq(test_url)
        link_called = true
      end

      quick_actions.process(thread)
      expect(link_called).to eq(true)
    end

    it 'creates a new sub-topic from a quick action in a thread' do
      sub_topic_title = "Sub-Topic"
      issue = TestUtils.new_issue(
        client,
        test_project,
        title: 'TestTitle',
        threads: [
          [ 'note1', 'note2', "/discuss sub-topic #{sub_topic_title}" ],
        ],
      )

      topics = loader.load_topics
      expect(topics.count).to eq(1)
      expect(topics[0].title).to eq('TestTitle')

      topic = topics.first
      expect(topic.threads.count).to eq(1)

      thread = topic.threads.first
      expect(thread.notes.count).to eq(3)

      quick_actions = Discoto::QuickActions.new(loader)
      quick_actions.process(thread)

      expect(quick_actions.new_sub_topics.count).to eq(1)
      new_topic = quick_actions.new_sub_topics.first
      expect(new_topic.title).to eq(sub_topic_title)

      loader.cache_clear!

      topics = loader.load_topics
      expect(topics.count).to eq(1)
      topics = topics.first.process
      expect(topics.count).to eq(2)

      topic_map = topics.to_h { |t| [t.title, t] }
      root_topic = topic_map['TestTitle']
      sub_topic1 = topic_map['Sub-Topic']

      root_thread = root_topic.threads.first
      expect(root_thread.notes.count).to eq(4)
      expect(root_thread.notes.last.body).to include(sub_topic1.url)
    end

    it 'does not allow infinite recursion creating sub topics' do
      sub_topic_title = "Sub-Topic"
      issue = TestUtils.new_issue(
        client,
        test_project,
        title: 'TestTitle',
        threads: [
          [
            'note1',
            'note2',
            # extra text in the note body caused the quick action to be copied
            # into the new thread, which triggers the recursion
            "QUICKACTION\n\n/discuss sub-topic #{sub_topic_title}"
          ],
        ],
      )

      # initial load
      topics = loader.load_topics
      expect(topics.count).to eq(1)
      expect(topics[0].title).to eq('TestTitle')

      topics = topics.first.process
      expect(topics.count).to eq(2)

      loader.cache_clear!
      # reload to see the new comments
      sub_topic = Discoto::Items::Topic.create_from_meta(loader, topics.last.meta)

      # now check the new topic's notes - no threads should be created
      expect(sub_topic.threads.length).to eq(0)
    end

    it 'create multiple sub-topics from quick actions in a single thread' do
      sub_topic_title = "Sub-Topic"
      sub_topic_title2 = "Sub-Topic2"
      issue = TestUtils.new_issue(
        client,
        test_project,
        title: 'TestTitle',
        threads: [
          [
            'note1',
            'note2',
            "/discuss sub-topic #{sub_topic_title}\n/discuss sub-topic #{sub_topic_title2}"
          ],
        ],
      )

      topics = loader.load_topics
      expect(topics.count).to eq(1)
      expect(topics[0].title).to eq('TestTitle')

      topic = topics.first
      expect(topic.threads.count).to eq(1)

      thread = topic.threads.first
      expect(thread.notes.count).to eq(3)

      quick_actions = Discoto::QuickActions.new(loader)
      quick_actions.process(thread)

      expect(quick_actions.new_sub_topics.count).to eq(2)
      new_topic = quick_actions.new_sub_topics[0]
      new_topic2 = quick_actions.new_sub_topics[1]
      expect(new_topic.title).to eq(sub_topic_title)
      expect(new_topic2.title).to eq(sub_topic_title2)

      loader.cache_clear!

      topics = loader.load_topics
      expect(topics.count).to eq(1)
      topics = topics.first.process
      expect(topics.count).to eq(3)

      topic_map = topics.to_h { |t| [t.title, t] }
      root_topic = topic_map['TestTitle']
      sub_topic1 = topic_map['Sub-Topic']
      sub_topic2 = topic_map['Sub-Topic2']

      root_thread = root_topic.threads.first
      expect(root_thread.notes.count).to eq(5)
      expect(root_thread.notes[3].body).to include(sub_topic1.url)
      expect(root_thread.notes[4].body).to include(sub_topic2.url)
    end

    it 'inserts a link in response to a link quick actions in a thread' do
      linked_topic_title = "Sub-Topic"
      issue_to_link = TestUtils.new_issue(
        client,
        test_project,
        title: linked_topic_title,
        labels: [], # not directly loaded by discoto
      )
      issue = TestUtils.new_issue(
        client,
        test_project,
        title: 'TestTitle',
        threads: [
          [
            'note1',
            'note2',
            "/discuss link #{issue_to_link.web_url}"
          ],
        ],
      )

      topics = loader.load_topics
      expect(topics.count).to eq(1)
      expect(topics[0].title).to eq('TestTitle')

      topic = topics.first
      expect(topic.threads.count).to eq(1)

      thread = topic.threads.first
      expect(thread.notes.count).to eq(3)

      quick_actions = Discoto::QuickActions.new(loader)
      quick_actions.process(thread)

      expect(quick_actions.new_sub_topics.count).to eq(1)
      new_topic = quick_actions.new_sub_topics.first
      expect(new_topic.title).to eq(linked_topic_title)

      loader.cache_clear!

      topics = loader.load_topics
      expect(topics.count).to eq(1)
      topics = topics.first.process
      expect(topics.count).to eq(2)

      topic_map = topics.to_h { |t| [t.title, t] }
      root_topic = topic_map['TestTitle']
      linked_topic = topic_map[linked_topic_title]

      root_thread = root_topic.threads.first
      expect(root_thread.notes.count).to eq(4)
      expect(root_thread.notes[3].body).to include(linked_topic.url)
    end

    it 'inserts links in response to multiple link quick actions in a thread' do
      linked_topic_title = "Sub-Topic"
      issue_to_link = TestUtils.new_issue(
        client,
        test_project,
        title: linked_topic_title,
        labels: [], # not directly loaded by discoto
      )
      linked_topic_title2 = "Sub-Topic2"
      issue_to_link2 = TestUtils.new_issue(
        client,
        test_project,
        title: linked_topic_title2,
        labels: [], # not directly loaded by discoto
      )
      issue = TestUtils.new_issue(
        client,
        test_project,
        title: 'TestTitle',
        threads: [
          [
            'note1',
            'note2',
            "/discuss link #{issue_to_link.web_url}\n\n/discuss link #{issue_to_link2.web_url}",
          ],
        ],
      )

      topics = loader.load_topics
      expect(topics.count).to eq(1)
      expect(topics[0].title).to eq('TestTitle')

      topic = topics.first
      expect(topic.threads.count).to eq(1)

      thread = topic.threads.first
      expect(thread.notes.count).to eq(3)

      quick_actions = Discoto::QuickActions.new(loader)
      quick_actions.process(thread)

      expect(quick_actions.new_sub_topics.count).to eq(2)
      new_topic = quick_actions.new_sub_topics.first
      expect(new_topic.title).to eq(linked_topic_title)
      new_topic2 = quick_actions.new_sub_topics.last
      expect(new_topic2.title).to eq(linked_topic_title2)

      loader.cache_clear!

      topics = loader.load_topics
      expect(topics.count).to eq(1)
      topics = topics.first.process
      expect(topics.count).to eq(3)

      topic_map = topics.to_h { |t| [t.title, t] }
      root_topic = topic_map['TestTitle']
      linked_topic = topic_map[linked_topic_title]
      linked_topic2 = topic_map[linked_topic_title2]

      root_thread = root_topic.threads.first
      expect(root_thread.notes.count).to eq(5)
      expect(root_thread.notes[3].body).to include(linked_topic.url)
      expect(root_thread.notes[4].body).to include(linked_topic2.url)
    end
  end

  describe '#self.quick_action?' do
    it 'detects existing quick actions in text' do
      data = <<~EOF
        Line 1
        Line 2

        /discuss an action
      EOF

      expect(Discoto::QuickActions.quick_action?(data)).to be(true)
    end

    it 'detects absence of quick actions in text' do
      data = <<~EOF
        Line 1
        Line 2
      EOF

      expect(Discoto::QuickActions.quick_action?(data)).to be(false)
    end

    it 'requires quick actions to be at the start of a line' do
      data = <<~EOF
        Line 1
          /discuss an action
        Line 2
      EOF

      expect(Discoto::QuickActions.quick_action?(data)).to be(false)
    end
  end

  describe '#self.find_quick_actions' do
    it 'Returns all quick actions in text' do
      data = <<~EOF
        Line 1
        Line 2
        /discuss action1
        Line 3
        /discuss action2
      EOF

      actions = Discoto::QuickActions.find_quick_actions(data)
      expect(actions.size).to be(2)
      expect(actions[0]).to eq("/discuss action1")
      expect(actions[1]).to eq("/discuss action2")
    end

    it 'Returns empty array if no quick actions exist' do
      data = <<~EOF
        Line 1
        Line 2
      EOF

      actions = Discoto::QuickActions.find_quick_actions(data)
      expect(actions.size).to be(0)
    end
  end

  describe '#self.remove_quick_actions' do
    it 'Replaces all quick actions with an empty string' do
      data = <<~EOF
        Line 1
        /discuss a discussion
        Line 2
      EOF
      expected = <<~EOF
        Line 1
        
        Line 2
      EOF

      new_text = Discoto::QuickActions.remove_quick_actions(data)
      expect(new_text).to eq(expected)
    end

    it 'Does not modify text that does not contain quick actions' do
      data = <<~EOF
        Line 1
        Line 2
      EOF

      new_text = Discoto::QuickActions.remove_quick_actions(data)
      expect(new_text).to eq(data)
    end
  end
end
