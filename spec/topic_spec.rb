# frozen_string_literal: true

require 'rspec'
require 'rspec/mocks'

require 'discoto/items/topic'

require_relative 'discoto_setup'

describe 'Topic' do
  include_context 'discoto setup'

  describe '#self.create' do
    it 'creates a new issue with the correct parameters' do
      title = 'TITLE'
      description = 'DESC'
      topic = Discoto::Items::Topic.create(loader, test_project.id, title, description: description)

      expect(topic.issuable.project_id).to eq(test_project.id)
      expect(topic.title).to eq(title)
      expect(topic.issuable.description).to eq(description)
    end
  end

  describe '#self.create_from_meta' do
    it 'loads an existing issue topic from a metadata hash' do
      issue = TestUtils.new_issue(client, test_project)
      meta = {
        'container_id' => test_project.id,
        'container_type' => 'project',
        'iid' => issue.iid,
        'issuable_type' => 'issue'
      }

      topic = Discoto::Items::Topic.create_from_meta(loader, meta)

      expect(topic.issuable.id).to eq(issue.id)
    end

    it 'loads an existing mr topic from a metadata hash' do
      mr = TestUtils.new_mr(client, test_project)
      meta = {
        'container_id' => test_project.id,
        'container_type' => 'project',
        'iid' => mr.iid,
        'issuable_type' => 'merge_request'
      }

      topic = Discoto::Items::Topic.create_from_meta(loader, meta)

      expect(topic.issuable.id).to eq(mr.id)
    end
  end

  describe '#initialize' do
    it 'creates a topic and loads all contained threads' do
      threads = [
        [ 'not', 'an', 'inline', 'topic' ]
      ]
      issue = TestUtils.new_issue(client, test_project, threads: threads)

      issuable = loader.load_issue(issue.iid, test_project.id)
      topic = Discoto::Items::Topic.new(loader, issuable)

      expect(topic.title).to eq(issuable.title)
      expect(topic.threads.count).to eq(threads.count)
      expect(topic.threads.first.notes.count).to eq(threads.first.count)
    end
  end

  describe '#process' do
    it 'Creates inline topics of all topic threads' do
      topic_titles = [ 'Topic1', 'Topic2', 'Topic3' ]
      threads = [
        [ "# Topic: #{topic_titles[0]}", "note1" ],
        [ "# Topic : #{topic_titles[1]}", "note2" ],
        [ "# Topic  - #{topic_titles[2]}", "note3" ],
      ]
      issue = TestUtils.new_issue(client, test_project, title: 'Root', threads: threads)

      topics = loader.load_topics
      expect(topics.count).to eq(1)
      topic = topics.first

      ts = topic.process
      expect(ts.count).to eq(4)

      expect(ts[0].title).to eq('Root')
      expect(ts[1].title).to eq(topic_titles[0])
      expect(ts[2].title).to eq(topic_titles[1])
      expect(ts[3].title).to eq(topic_titles[2])
    end

    it 'Causes quick actions to be evaluated in all scenarios' do
      expect(client.issues(test_project.id, { state: 'opened' }).count).to eq(0)

      inline_titles = ["Inline1", "Inline2"]
      sub_titles = ["SUB TITLE", "SUB TITLE2", "SUB TITLE3"]
      threads = [
        [ "/discuss sub-topic #{sub_titles[0]}" ],
        [ "## Topic: #{inline_titles[0]}\n\n/discuss sub-topic #{sub_titles[1]}" ],
        [ "## Topic: #{inline_titles[1]}", "/discuss sub-topic #{sub_titles[2]}" ],
      ]
      issue = TestUtils.new_issue(client, test_project, title: 'Root', threads: threads)

      topics = loader.load_topics
      expect(topics.count).to eq(1)
      topic = topics.first

      ts = topic.process
      expect(ts.count).to eq(6) # 1 root, two inlines, three newly-created sub-topics

      expect(ts[0].title).to eq('Root')
      expect(ts[1].title).to eq(inline_titles[0])
      expect(ts[2].title).to eq(sub_titles[1])
      expect(ts[3].title).to eq(inline_titles[1])
      expect(ts[4].title).to eq(sub_titles[2])
      expect(ts[5].title).to eq(sub_titles[0])
    end

    it 'Does not allow duplicated sub-topics' do
      linked_issue = TestUtils.new_issue(client, test_project, labels: [])
      root_issue = TestUtils.new_issue(client, test_project, title: 'Root', threads: [
        # link it twice
        [ "/discuss link #{linked_issue.web_url}" ],
        [ "/discuss link #{linked_issue.web_url}" ],
      ])

      topics = loader.load_topics
      expect(topics.count).to eq(1)

      # will process quick actions, insert json meta links, etc.
      # Verify at this stage...
      all_topics = topics.first.process
      expect(all_topics.count).to eq(2)
      root_topic = all_topics.first
      expect(root_topic.title).to eq('Root')
      expect(root_topic.sub_topics.count).to eq(1) # 2 == it allowed a duplicated link

      # .. and at this stage (simulate the next "run" of discoto after json meta
      # data has already been added)
      loader.cache_clear!

      topics = loader.load_topics
      expect(topics.count).to eq(1)

      # all quick actions should have already been taken care of
      all_topics = topics.first.process
      expect(all_topics.count).to eq(2)
      root_topic = all_topics.first
      expect(root_topic.title).to eq('Root')
      expect(root_topic.sub_topics.count).to eq(1) # 2 == it allowed a duplicated link
    end
  end

  describe '#main_points' do
    # excluding inline topics!
    it 'Returns the main points directly part of this topic' do
      points = [ "point1", "point2", "point3" ]
      issue = TestUtils.new_issue(client, test_project, threads: [
        [ '# Topic: Inline Topic', "* point: not included" ],
        [ "* point: #{points[0]}", "point: #{points[1]}" ],
        [ "* point: #{points[2]}" ],
      ])

      topics = loader.load_topics
      expect(topics.count).to eq(1)
      topic = topics.first
      topic.process

      expect(topic.main_points.map(&:text)).to eq(points)
    end

    it 'Returns the same result every time' do
      issue = TestUtils.new_issue(client, test_project, threads: [
        [ '# POINT: point0' ],
        [ '# POINT: point1' ],
        [ '# POINT: point2' ],
        [ '# POINT: point3' ],
      ])

      topics = loader.load_topics
      expect(topics.count).to eq(1)

      all_topics = topics.first.process
      expect(all_topics.count).to eq(1)

      topic = all_topics.first
      expect(topic.main_points.count).to eq(4)
      10.times do
        expect(topic.main_points.count).to eq(4)
      end
    end
  end

  it 'Allows variants of point declarations' do
      points = [ "point1", "point2", "point3", "point4" ]
      issue = TestUtils.new_issue(client, test_project, threads: [
        [ "* point:#{points[0]}"],
        [ "* point    :      #{points[1]}"],
        [ "point-#{points[2]}" ],
        [ "* point    -      #{points[3]}" ],
      ])

      topics = loader.load_topics
      expect(topics.count).to eq(1)
      topic = topics.first
      topic.process

      expect(topic.main_points.map(&:text)).to eq(points)
  end

  describe '#summary_lines' do
    it 'Calculates the nested list of points and topics' do
      points = [ "point1", "point2", "point3" ]
      nested_topic = "Inline Topic"
      nested_point = "A nested point"
      issue = TestUtils.new_issue(client, test_project, threads: [
        [ "# Topic: #{nested_topic}", "* point: #{nested_point}" ],
        [ "* point: #{points[0]}", "point: #{points[1]}" ],
        [ "* point: #{points[2]}" ],
      ])

      topics = loader.load_topics
      expect(topics.count).to eq(1)
      topic = topics.first
      topic.process

      lines = topic.summary_lines
      expect(lines.count).to eq(5)
      expect(lines[0]).to start_with("* **TOPIC** #{nested_topic}")
      expect(lines[1]).to start_with("  * #{nested_point}")
      expect(lines[2]).to start_with("* #{points[0]}")
      expect(lines[3]).to start_with("* #{points[1]}")
      expect(lines[4]).to start_with("* #{points[2]}")
    end

    it 'Calculates a correct summary of linked topics' do
      linked_points = [ "link point1", "link point2", "link point3" ]
      linked_topic_title = "Linked Topic"
      linked_nested_topic = "Linked Nested"
      linked_nested_point = "Linked Nested Point"
      linked_issue = TestUtils.new_issue(client, test_project, title: linked_topic_title, labels: [], threads: [
        [ "# Topic: #{linked_nested_topic}", "* point: #{linked_nested_point}" ],
        linked_points.map { |p| "* point: #{p}" }
      ])

      points = [ "point1", "point2" ]
      issue = TestUtils.new_issue(client, test_project, threads: [
        points.map { |p| "* point: #{p}" },
        [ "/discuss link #{linked_issue.web_url}" ]
      ])

      topics = loader.load_topics
      expect(topics.count).to eq(1)
      root_topic = topics.first
      all_topics = root_topic.process

      expect(all_topics.count).to eq(3)
      linked_topic = all_topics[1]
      linked_inline_topic = all_topics[2]

      root_lines = root_topic.summary_lines
      expect(root_lines[0]).to start_with("* **TOPIC** #{linked_topic_title}")
      expect(root_lines[1]).to start_with("  * **TOPIC** #{linked_nested_topic}")
      expect(root_lines[2]).to start_with("    * #{linked_nested_point}")
      expect(root_lines[3]).to start_with("  * #{linked_points[0]}")
      expect(root_lines[4]).to start_with("  * #{linked_points[1]}")
      expect(root_lines[5]).to start_with("  * #{linked_points[2]}")
      expect(root_lines[6]).to start_with("* #{points[0]}")
      expect(root_lines[7]).to start_with("* #{points[1]}")

      linked_lines = linked_topic.summary_lines
      expect(linked_lines[0]).to start_with("* **TOPIC** #{linked_nested_topic}")
      expect(linked_lines[1]).to start_with("  * #{linked_nested_point}")
      expect(linked_lines[2]).to start_with("* #{linked_points[0]}")
      expect(linked_lines[3]).to start_with("* #{linked_points[1]}")
      expect(linked_lines[4]).to start_with("* #{linked_points[2]}")
    end
  end

  describe '#update_summary' do
    it 'Updates summary descriptions in the issuable description' do
      points = [ "point1", "point2", "point3" ]
      nested_topic = "Inline Topic"
      nested_point = "A nested point"
      issue = TestUtils.new_issue(client, test_project, threads: [
        [ "# Topic: #{nested_topic}", "* point: #{nested_point}" ],
        [ "* point: #{points[0]}", "point: #{points[1]}" ],
        [ "* point: #{points[2]}" ],
      ])

      topics = loader.load_topics
      expect(topics.count).to eq(1)
      topic = topics.first
      topic.process
      topic.update_summary

      expect(topic.updated_summary).to eq(true)
      expect(topic.issuable.description).to include(Discoto::Items::SUMMARY_PREFIX)
      expect(topic.issuable.description).to include(Discoto::Items::SUMMARY_POSTFIX)
      expect(topic.issuable.description).to include('Discoto Usage')
      expect(topic.issuable.description).to include(points[0])
      expect(topic.issuable.description).to include(points[1])
      expect(topic.issuable.description).to include(points[2])
      expect(topic.issuable.description).to include(nested_point)
    end

    it 'Adds root topic links in the issuable description' do
      root_title = "ROOT TITLE"
      root_issue = TestUtils.new_issue(client, test_project, title: root_title, threads: [
        [ '/discuss sub-topic A Sub Topic' ],
      ])

      topics = loader.load_topics
      expect(topics.count).to eq(1)
      all_topics = topics.first.process

      root_topics = all_topics[0]
      sub_topic = all_topics[1]

      sub_topic.update_summary
      desc = sub_topic.issuable.description
      expect(desc).to include("**ROOT** #{root_title}")
      expect(desc).to_not include("**PARENT**")
    end

    it 'Adds root topic links in the issuable description' do
      linked_title = "Linked Issue"
      linked_issue = TestUtils.new_issue(
        client,
        test_project,
        labels: [],
        title: linked_title,
        threads: [
          [ '/discuss sub-topic A Sub Topic' ],
        ]
      )

      root_title = "ROOT TITLE"
      root_issue = TestUtils.new_issue(client, test_project, title: root_title, threads: [
        [ "/discuss link #{linked_issue.web_url}" ]
      ])

      topics = loader.load_topics
      expect(topics.count).to eq(1)

      all_topics = topics.first.process
      expect(all_topics.count).to eq(3)

      root_topic = all_topics[0]
      linked_topic = all_topics[1]
      sub_topic = all_topics[2]

      sub_topic.update_summary
      desc = sub_topic.issuable.description
      expect(desc).to include("**ROOT** #{root_title}")
      expect(desc).to include("**PARENT** #{linked_title}")
    end

    it 'Does not allow confidential topic text to propagate into public' do
      confidential_point = "Confidential Point"
      linked_title = "Confidential Issue"
      linked_issue = TestUtils.new_issue(
        client,
        test_project,
        labels: [],
        title: linked_title,
        confidential: true,
        threads: [ [ "* point: #{confidential_point}" ] ],
      )

      root_title = "ROOT TITLE"
      root_issue = TestUtils.new_issue(client, test_project, title: root_title, threads: [
        [ "/discuss link #{linked_issue.web_url}" ]
      ])

      topics = loader.load_topics
      expect(topics.count).to eq(1)
      topics = topics[0].process

      topics.map(&:update_summary)

      root_topic = topics[0]
      expect(root_topic.title).to eq(root_title)
      confidential_topic = topics[1]

      desc = root_topic.issuable.description
      expect(desc).not_to include(linked_title)
      expect(desc).not_to include(confidential_point)
    end

    it 'Does not allow confidential topic text from private project to propagate into public' do
      confidential_point = "Confidential Point"
      linked_title = "Confidential Issue"
      linked_issue = TestUtils.new_issue(
        client,
        test_project_private,
        labels: [],
        title: linked_title,
        confidential: true,
        threads: [ [ "* point: #{confidential_point}" ] ],
      )

      root_title = "ROOT TITLE"
      root_issue = TestUtils.new_issue(client, test_project, title: root_title, threads: [
        [ "/discuss link #{linked_issue.web_url}" ]
      ])

      topics = loader.load_topics
      expect(topics.count).to eq(1)
      topics = topics[0].process

      topics.map(&:update_summary)

      root_topic = topics[0]
      expect(root_topic.title).to eq(root_title)
      confidential_topic = topics[1]

      desc = root_topic.issuable.description
      expect(desc).not_to include(linked_title)
      expect(desc).not_to include(confidential_point)
    end

    it 'Does not allow confidential root topic titles to be shown in public sub topics' do
      child_title = 'Child Issue'
      child_issue = TestUtils.new_issue(
        client,
        test_project,
        title: child_title, labels: [])

      parent_title = 'Parent Issue'
      parent_issue = TestUtils.new_issue(
        client,
        test_project,
        title: parent_title,
        labels: [],
        threads: [ [ "/discuss link #{child_issue.web_url}" ] ]
      )

      root_title = "Confidential Root"
      root_issue = TestUtils.new_issue(
        client,
        test_project,
        title: root_title,
        confidential: true,
        threads: [ [ "/discuss link #{parent_issue.web_url}" ] ]
      )

      topics = loader.load_topics
      expect(topics.count).to eq(1)
      all_topics = topics[0].process
      expect(all_topics.count).to eq(3)

      root_topic = all_topics[0]
      expect(root_topic.title).to eq(root_title)
      parent_topic = all_topics[1]
      expect(parent_topic.title).to eq(parent_title)
      child_topic = all_topics[2]
      expect(child_topic.title).to eq(child_title)

      all_topics.map(&:update_summary)

      expect(child_topic.issuable.description).to_not include(root_title)
    end

    it 'Does not allow confidential parent topic titles to be shown in public sub topics' do
      child_title = 'Child Issue'
      child_issue = TestUtils.new_issue(
        client,
        test_project,
        title: child_title, labels: [])

      parent_title = 'Parent Issue'
      parent_issue = TestUtils.new_issue(
        client,
        test_project,
        title: parent_title,
        labels: [],
        confidential: true,
        threads: [ [ "/discuss link #{child_issue.web_url}" ] ]
      )

      root_title = "Confidential Root"
      root_issue = TestUtils.new_issue(
        client,
        test_project,
        title: root_title,
        threads: [ [ "/discuss link #{parent_issue.web_url}" ] ]
      )

      topics = loader.load_topics
      expect(topics.count).to eq(1)
      all_topics = topics[0].process
      expect(all_topics.count).to eq(3)

      root_topic = all_topics[0]
      expect(root_topic.title).to eq(root_title)
      parent_topic = all_topics[1]
      expect(parent_topic.title).to eq(parent_title)
      child_topic = all_topics[2]
      expect(child_topic.title).to eq(child_title)

      all_topics.map(&:update_summary)

      expect(child_topic.issuable.description).to include(root_title)
      expect(child_topic.issuable.description).to_not include(parent_title)
    end
  end

  describe '#break_cycles!' do
    it 'Breaks cyclic links' do
      issue1 = TestUtils.new_issue(client, test_project, threads: [
        [ '/discuss sub-topic issue2' ]
      ])

      topics = loader.load_topics
      expect(topics.count).to eq(1)
      
      topic1, topic2 = topics.first.process
      topic2.create_thread("A").add_note('/discuss sub-topic issue3')

      loader.cache_clear!

      topics = loader.load_topics
      expect(topics.count).to eq(1)

      topic1, topic2, topic3 = topics.first.process
      topic3.create_thread("A").add_note("/discuss link #{topic1.url}")

      loader.cache_clear!

      topics = loader.load_topics
      expect(topics.count).to eq(1)

      all_topics = topics.first.process
      topic1, topic2, topic3 = all_topics

      expect(topic1.parent_topic).to eq(topic3)
      expect(topic2.parent_topic).to eq(topic1)
      expect(topic3.parent_topic).to eq(topic2)

      all_topics.map(&:break_cycles!)

      expect(topic1.parent_topic).to eq(nil)
      expect(topic2.parent_topic).to eq(topic1)
      expect(topic3.parent_topic).to eq(topic2)
    end
  end
end
