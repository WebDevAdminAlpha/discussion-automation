# frozen_string_literal: true


module TestUtils
  def self.new_project(client, group, name)
    client.create_project(name, { namespace_id: group.id })
  end

  def self.new_issue(client, project, title: 'Title', description: nil, threads: nil, labels: nil, confidential: false)
    description ||= "Description"
    threads ||= []
    labels ||= ['discoto']

    new_issue = client.create_issue(project.id, title, {labels: labels, description: description, confidential: confidential})
    create_threads(client, project, 'issue', new_issue, threads)

    # now wait until we can fetch it separately from the API
    begin
      tmp = client.issue(project.id, new_issue.iid)
    rescue Gitlab::Error::NotFound
      sleep(0.5)
      retry
    end

    new_issue
  end

  def self.new_mr(client, project, title: 'Title', description: nil, threads: nil, labels: nil)
    description ||= "Description"
    threads ||= []
    labels ||= ['discoto']

    # make a new file on a new branch
    branch_name = "new-branch-#{Random.rand}"
    client.create_branch(project.id, branch_name, 'master')
    begin
      client.create_file(project.id, 'new-file', branch_name, "contents", "commit-message")
    rescue Gitlab::Error::BadRequest
      sleep 0.5
      retry
    end

    new_mr = client.create_merge_request(project.id, title, {
      source_branch: branch_name,
      target_branch: "master",
      description: description,
      labels: labels,
    })
    create_threads(client, project, 'merge_request', new_mr, threads)

    # now wait until we can fetch it separately from the API
    begin
      tmp = client.merge_request(project.id, new_mr.iid)
    rescue Gitlab::Error::NotFound
      sleep(0.5)
      retry
    end

    new_mr
  end

  def self.create_threads(client, project, type, noteable, threads)
    threads.each do |thread|
      disc = nil
      thread.each do |note|
        if disc.nil?
          disc = create_project_discussion(client, project, type, noteable, note)
        else
          add_note_to_project_discussion(client, project, type, noteable, disc, note)
        end
      end
    end
  end

  def self.create_project_discussion(client, project, type, noteable, body)
    path = "/projects/#{project.id}/#{type}s/#{noteable.iid}/discussions"
    args = URI.encode_www_form('body' => body)
    client.post("#{path}?#{args}")
  end

  def self.add_note_to_project_discussion(client, project, type, noteable, discussion, body)
    path = "/projects/#{project.id}/#{type}s/#{noteable.iid}/discussions/#{discussion.id}/notes"
    args = URI.encode_www_form('body' => body)
    client.post("#{path}?#{args}")
  end
end
